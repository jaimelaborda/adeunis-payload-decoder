[
    {
        "id": "9c1edf3.64f122",
        "type": "lora in",
        "z": "9095711c.0a25a",
        "name": "",
        "datatype": "bytes",
        "x": 126,
        "y": 405,
        "wires": [
            [
                "2a0528d6.a617a8",
                "7a82c80c.50fe58"
            ]
        ]
    },
    {
        "id": "7a82c80c.50fe58",
        "type": "function",
        "z": "9095711c.0a25a",
        "name": "FTD Decoder",
        "func": "function Decoder(bytes, port) {\n  // Functions\n  function parseCoordinate(raw_value, coordinate) {\n    // This function parses a coordinate payload part into \n    // dmm and ddd \n    var raw_itude = raw_value;\n    var temp = \"\";\n\n    // Degree section\n    var itude_string = ((raw_itude >> 28) & 0xF).toString();\n    raw_itude <<= 4;\n\n    itude_string += ((raw_itude >> 28) & 0xF).toString();\n    raw_itude <<= 4;\n\n    coordinate.degrees += itude_string;\n    itude_string += \"°\";\n\n    // Minute section\n    temp = ((raw_itude >> 28) & 0xF).toString();\n    raw_itude <<= 4;\n\n    temp += ((raw_itude >> 28) & 0xF).toString();\n    raw_itude <<= 4;\n    itude_string += temp;\n    itude_string += \".\";\n    coordinate.minutes += temp;\n\n    // Decimal section\n    temp = ((raw_itude >> 28) & 0xF).toString();\n    raw_itude <<= 4;\n\n    temp += ((raw_itude >> 28) & 0xF).toString();\n    raw_itude <<= 4;\n\n    itude_string += temp;\n    coordinate.minutes += \".\";\n    coordinate.minutes += temp;\n\n    return itude_string;\n  }\n\n  function parseLatitude(raw_latitude, coordinate) {\n    var latitude = parseCoordinate(raw_latitude, coordinate);\n    latitude += ((raw_latitude & 0xF0) >> 4).toString();\n    coordinate.minutes += ((raw_latitude & 0xF0) >> 4).toString();\n\n    return latitude;\n  }\n\n  function parseLongitude(raw_longitude, coordinate) {\n    var longitude = (((raw_longitude >> 28) & 0xF)).toString();\n    coordinate.degrees = longitude;\n\n    longitude += parseCoordinate(raw_longitude << 4, coordinate);\n\n    return longitude;\n  }\n\n  function addField(field_no, payload) {\n    switch (field_no) {\n      // Presence of temperature information\n      case 0:\n        payload.temperature = bytes[bytes_pos_] & 0x7F;\n        // Temperature is negative\n        if ((bytes[bytes_pos_] & 0x80) > 0) {\n          payload.temperature -= 128;\n        }\n        bytes_pos_++;\n        break;\n        // Transmission triggered by the accelerometer\n      case 1:\n        payload.trigger = \"accelerometer\";\n        break;\n        // Transmission triggered by pressing pushbutton 1\n      case 2:\n        payload.trigger = \"pushbutton\";\n        break;\n        // Presence of GPS information\n      case 3:\n        // GPS Latitude\n        // An object is needed to handle and parse coordinates into ddd notation\n        var coordinate = {};\n        coordinate.degrees = \"\";\n        coordinate.minutes = \"\";\n\n        var raw_value = 0;\n        raw_value |= bytes[bytes_pos_++] << 24;\n        raw_value |= bytes[bytes_pos_++] << 16;\n        raw_value |= bytes[bytes_pos_++] << 8;\n        raw_value |= bytes[bytes_pos_++];\n\n        payload.lati_hemisphere = (raw_value & 1) == 1 ? \"South\" : \"North\";\n        payload.latitude_dmm = payload.lati_hemisphere.charAt(0) + \" \";\n        payload.latitude_dmm += parseLatitude(raw_value, coordinate);\n        payload.latitude = (parseFloat(coordinate.degrees) + parseFloat(coordinate.minutes) / 60) * ((raw_value & 1) == 1 ? -1.0 : 1.0);\n\n        // GPS Longitude\n        coordinate.degrees = \"\";\n        coordinate.minutes = \"\";\n        raw_value = 0;\n        raw_value |= bytes[bytes_pos_++] << 24;\n        raw_value |= bytes[bytes_pos_++] << 16;\n        raw_value |= bytes[bytes_pos_++] << 8;\n        raw_value |= bytes[bytes_pos_++];\n\n        payload.long_hemisphere = (raw_value & 1) == 1 ? \"West\" : \"East\";\n        payload.longitude_dmm = payload.long_hemisphere.charAt(0) + \" \";\n        payload.longitude_dmm += parseLongitude(raw_value, coordinate);\n        payload.longitude = (parseFloat(coordinate.degrees) + parseFloat(coordinate.minutes) / 60) * ((raw_value & 1) == 1 ? -1.0 : 1.0);\n\n        // GPS Quality\n        raw_value = bytes[bytes_pos_++];\n        switch ((raw_value & 0xF0) >> 4) {\n          case 1:\n            payload.gps_quality = \"Good\";\n            break;\n          case 2:\n            payload.gps_quality = \"Average\";\n            break;\n          case 3:\n            payload.gps_quality = \"Poor\";\n            break;\n          default:\n            payload.gps_quality = (raw_value >> 4) & 0xF;\n            break;\n        }\n        payload.hdop = (raw_value >> 4) & 0xF;\n\n        // Number of satellites\n        payload.sats = raw_value & 0xF;\n        break;\n\n        // Presence of Uplink frame counter\n      case 4:\n        payload.ul_counter = bytes[bytes_pos_++];\n        break;\n\n        // Presence of Downlink frame counter\n      case 5:\n        payload.dl_counter = bytes[bytes_pos_++];\n        break;\n\n        // Presence of battery level information\n      case 6:\n        payload.battery_level = bytes[bytes_pos_++] << 8;\n        payload.battery_level |= bytes[bytes_pos_++];\n        break;\n        // Presence of RSSI and SNR information\n      case 7:\n        // RSSI\n        payload.rssi_dl = bytes[bytes_pos_++];\n        payload.rssi_dl *= -1;\n\n        // SNR\n        payload.snr_dl = bytes[bytes_pos_] & 0x7F;\n        if ((bytes[bytes_pos_] & 0x80) > 0) {\n          payload.snr_dl -= 128;\n        }\n        bytes_pos_++;\n        break;\n\n      default:\n        // Do nothing\n        break;\n    }\n  }\n\n  // Declaration & initialization\n  var status_ = bytes[0];\n  var bytes_len_ = bytes.length;\n  var bytes_pos_ = 1;\n  var i = 0;\n  var payload = {};\n\n  // Get raw payload\n  var temp_hex_str = \"\"\n  payload.payload = \"\";\n  for (var j = 0; j < bytes_len_; j++) {\n    temp_hex_str = bytes[j].toString(16).toUpperCase();\n    if (temp_hex_str.length == 1) {\n      temp_hex_str = \"0\" + temp_hex_str;\n    }\n    payload.payload += temp_hex_str;\n  }\n\n  // Get payload values\n  do {\n    // Check status, whether a field is set\n    if ((status_ & 0x80) > 0) {\n      addField(i, payload);\n    }\n    i++;\n  }\n  while (((status_ <<= 1) & 0xFF) > 0);\n  return payload;\n}\n\n//Get the payload from argument\nvar data_received = msg.payload;\n//var bytes = data_received.match(/.{2}/g);\n\n//bytes_out = [];\n\n//for(var i=0; i<bytes.length; i++){\n//  bytes_out[i] = parseInt(bytes[i], 16);\n  //console.log(typeof(bytes_out[i])+' '+bytes_out[i]);\n//}\n\n//var decodified_data = {};\ndecodified_data = Decoder(data_received, 0);\n\nmsg.payload = decodified_data;\n\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 433,
        "y": 406,
        "wires": [
            [
                "ebc1573f.6724d8",
                "73254f0d.a228e"
            ]
        ]
    },
    {
        "id": "ebc1573f.6724d8",
        "type": "debug",
        "z": "9095711c.0a25a",
        "name": "",
        "active": false,
        "console": "false",
        "complete": "payload",
        "x": 601,
        "y": 319,
        "wires": []
    },
    {
        "id": "2a0528d6.a617a8",
        "type": "debug",
        "z": "9095711c.0a25a",
        "name": "",
        "active": false,
        "console": "false",
        "complete": "payload",
        "x": 312,
        "y": 317,
        "wires": []
    },
    {
        "id": "dfaaba23.7ccc68",
        "type": "file",
        "z": "9095711c.0a25a",
        "name": "field_test.csv",
        "filename": "/home/admin/field_test.csv",
        "appendNewline": true,
        "createDir": true,
        "overwriteFile": "false",
        "x": 923,
        "y": 406,
        "wires": []
    },
    {
        "id": "73254f0d.a228e",
        "type": "function",
        "z": "9095711c.0a25a",
        "name": "Add timestamp",
        "func": "const now = new Date().toLocaleString('es-ES', {\n    timeZone: 'Europe/Madrid'\n});\n\nmsg.payload.time = now;\n\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 689,
        "y": 406,
        "wires": [
            [
                "95a0b4c.a1f6648",
                "dfaaba23.7ccc68"
            ]
        ]
    },
    {
        "id": "95a0b4c.a1f6648",
        "type": "debug",
        "z": "9095711c.0a25a",
        "name": "",
        "active": true,
        "console": "false",
        "complete": "payload",
        "x": 912,
        "y": 319,
        "wires": []
    },
    {
        "id": "d6c4eabe.a5c698",
        "type": "comment",
        "z": "9095711c.0a25a",
        "name": "Convert JSON Output to CSV",
        "info": "Para convertir el JSON que genera este flow a CSV y poder analizar los datos más facilmente usar:\nhttps://sqlify.io/convert",
        "x": 976,
        "y": 461,
        "wires": []
    }
]